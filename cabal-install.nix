{ stdenv, lib, runCommand, fetchurl, fetchFromGitHub, haskell, python3, zlib }: ghcs: conf:

let
  src = fetchFromGitHub {
    owner = "haskell";
    repo = "Cabal";
    rev = conf.version;
    sha256 = conf.sha256;
  };

  plan = lib.importJSON "${src}/bootstrap/linux-${conf.bootVer}.json";

  sources = lib.listToAttrs (lib.concatMap (dep:
      [
        { name = "${dep.package}-${dep.version}.tar.gz";
          value = fetchurl {
            url = "https://hackage.haskell.org/package/${dep.package}/${dep.package}-${dep.version}.tar.gz";
            sha256 = dep.src_sha256;
          };
        }
      ] ++ lib.optional (dep.revision != null) {
        name = "${dep.package}.cabal";
        value = fetchurl {
          url = "https://hackage.haskell.org/package/${dep.package}-${dep.version}/revision/${toString dep.revision}.cabal";
          sha256 = dep.cabal_sha256;
        };
      }
    )
    (lib.filter (dep: dep.src_sha256 != null) plan.dependencies));

  sourcesDir = runCommand "sources" {} (
    ''
      mkdir -p $out;
    '' +
    lib.concatStringsSep "\n" (lib.mapAttrsToList (name: src: "cp ${src} $out/${name};") sources)
  );

  cabal-install = stdenv.mkDerivation {
    name = "cabal-install";
    inherit src;
    nativeBuildInputs = [ ghcs.${conf.bootVer} python3 ];
    buildInputs = [ zlib ];
    patchPhase = ''
      mkdir -p _build
      ln -s ${sourcesDir} _build/tarballs
      ls -ls _build _build/tarballs _build/tarballs/*
    '';
    buildPhase = ''
      python3 bootstrap/bootstrap.py -d bootstrap/linux-${conf.bootVer}.json
    '';
    installPhase = ''
      mkdir -p $out/bin
      mv _build/bin/cabal $out/bin/cabal
    '';
  };

in cabal-install
