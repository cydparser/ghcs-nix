let
  sources = import ./nix/sources.nix;
  nixpkgs = import sources.nixpkgs {};
in with nixpkgs;

let
  artefacts = import "${sources.ghc-artefact-nix.outPath}/releases.nix" { baseNixpkgs = pkgs; };

  fetchGhc =
    { version, sha256
    , url ? null
    }: fetchTarball {
      url = if url != null then url else "https://downloads.haskell.org/ghc/${version}/ghc-${version}-src.tar.xz";
      inherit sha256;
    };

  fetchGhcGit = { ref, branch ? "master", sha256 }: fetchgit {
      url = "https://gitlab.haskell.org/ghc/ghc";
      rev = ref;
      branchName = branch;
      inherit sha256;
      fetchSubmodules = true;
  };

  # Patches applied to all GHC builds of the given version
  patchesForGhc =
    version:
      lib.optional (builtins.compareVersions version "8.10.5" < 0) ./patches/T19655.patch;

  buildGhc = {
    version, url ? null, sha256 ? null,
    src ? (fetchGhc { inherit url version sha256; }),
    patches ? [],
    bootGhc,
    werror ? false,
    sphinx ? null, #python3Packages.sphinx,
    happy ? haskellPackages.happy,
    alex ? haskellPackages.alex,
    hscolour ? haskellPackages.hscolour
  }:
    callPackage ./ghc.nix {
      inherit version src;
      patches = patches ++ patchesForGhc version;
      buildLlvmPackages = llvmPackages;
      inherit bootGhc werror sphinx happy alex hscolour;
    };

  buildGhcHadrian = {
    version, url ? null, sha256 ? null,
    src ? (fetchGhc { inherit url version sha256; }),
    patches ? [],
    bootGhc,
    hadrianBootstrapOpts ? null,
    werror ? false,
    sphinx ? python3Packages.sphinx,
    happy ? haskellPackages.happy,
    alex ? haskellPackages.alex,
    hscolour ? haskellPackages.hscolour,
    libffi ? pkgs.libffi,
    buildFlavour ? (if builtins.compareVersions version "9.4" < 0 then "perf" else "release")
  }: callPackage ./hadrian.nix {
    inherit version src;
    patches = patches ++ patchesForGhc version;
    buildLlvmPackages = llvmPackages;
    inherit bootGhc sphinx libffi alex happy hscolour hadrianBootstrapOpts buildFlavour;
  };

  happy-1_20_0 =
    let src = fetchTarball {
      url = "http://hackage.haskell.org/package/happy-1.20.0/happy-1.20.0.tar.gz";
      sha256 = "sha256:095qp013kkppgy2vw0vi3hkahbdif3mas3ryb06psn2vbqd5b47l";
    };
    in haskellPackages.callCabal2nix "happy" src {};

  alex-3_2_7 =
    let src = fetchTarball {
      url = "http://hackage.haskell.org/package/alex-3.2.7/alex-3.2.7.tar.gz";
      sha256 = "sha256:01jn15f7ma0npdm12bfbn6gnxc9nrd0gkk43kph4xszvyrb843hh";

    };
    in haskellPackages.callCabal2nix "alex" src {};

  ghcs =
    lib.filterAttrs (n: _: n != "master" && lib.versionOlder n "9.4") ghcs_make //
    lib.filterAttrs (n: _: n == "master" || lib.versionAtLeast n "9.4") ghcs_hadrian;

  ghcs_make = {
    #
    # GHC 8.2
    #
    "8.2.1" = buildGhc {
      version = "8.2.1";
      sha256 = "sha256:12lwazpfxiv7c6rjiz54w8wrc9fdjziiy9vvdm9g31wfdgazmaih";
      bootGhc = artefacts.ghc_8_2_2;
      sphinx = null; # Sphinx incompatible
    };
    "8.2.2" = buildGhc {
      version = "8.2.2";
      sha256 = "sha256:10l49pyha8gslvdl7v2kb879knqynnsqa4r9d2cysdqw6i6lcal5";
      bootGhc = artefacts.ghc_8_2_2;
      sphinx = null; # Sphinx incompatible
    };

    #
    # GHC 8.4
    #
    "8.4.1" = buildGhc {
      version = "8.4.1";
      sha256 = "sha256:1i8p2l654pm1qsjg8z01mm29rhyqqzx2klm3daqwspql5b3chnna";
      bootGhc = artefacts.ghc_8_2_2;
      sphinx = null; # Sphinx incompatible
    };
    "8.4.2" = buildGhc {
      version = "8.4.2";
      sha256 = "sha256:0d19cq7rmrbnv0dabxgcf9gadjas3f02wvighdfgr6zqr1z5fcrc";
      bootGhc = artefacts.ghc_8_2_2;
      sphinx = null; # Sphinx incompatible
    };
    "8.4.3" = buildGhc {
      version = "8.4.3";
      sha256 = "sha256:1y8bd6qxi5azqwyr930val428r2yi9igfprv11acd02g7d766yxq";
      bootGhc = artefacts.ghc_8_2_2;
      sphinx = null; # Sphinx incompatible
    };

    #
    # GHC 8.6
    #
    "8.6.2" = buildGhc {
      version = "8.6.2";
      sha256 = "sha256:1spb0jfxv3r0f12z6ys1y1ssnqwnnqavr947gnz7a74y6k8kb2h2";
      bootGhc = artefacts.ghc_8_6_5;
    };
    "8.6.3" = buildGhc {
      version = "8.6.4";
      sha256 = "sha256:1iz7lsw12vkl6da7cqbxiy5hidkfp3427fm30qv3294pvx2c2szr";
      bootGhc = artefacts.ghc_8_6_5;
    };
    "8.6.4" = buildGhc {
      version = "8.6.4";
      sha256 = "sha256:1iz7lsw12vkl6da7cqbxiy5hidkfp3427fm30qv3294pvx2c2szr";
      bootGhc = artefacts.ghc_8_6_5;
    };
    "8.6.5" = buildGhc {
      version = "8.6.5";
      sha256 = "sha256:0p7ykswxid024aqq0aqd91yla719kc1rnb5f90ply43xk9457687";
      bootGhc = artefacts.ghc_8_6_5;
    };

    #
    # GHC 8.8
    #
    "8.8.1" = buildGhc {
      version = "8.8.1";
      sha256 = "sha256:07ag4ah5dd65l6kxrrx1k9zxw0fswy2ias5kqs61rxif00a982jb";
      bootGhc = artefacts.ghc_8_6_5;
    };
    "8.8.2" = buildGhc {
      version = "8.8.2";
      sha256 = "sha256:1qf4nrrxn0fnfvxri5m34nx6f3x2px85xhxnypapa4j3y3dh5xvj";
      bootGhc = artefacts.ghc_8_6_5;
    };
    "8.8.3" = buildGhc {
      version = "8.8.3";
      sha256 = "sha256:1xmdrr6cj4i14ginfnprkrj5fh6d621r70fb93m8a72hk6ca9alg";
      bootGhc = artefacts.ghc_8_6_5;
    };
    "8.8.4" = buildGhc {
      version = "8.8.4";
      sha256 = "sha256:1vbgjxpl7s4nyfzmdzxy7zcpdzlhnn1zalb497p2w7bf0qamzmdq";
      bootGhc = artefacts.ghc_8_6_5;
    };

    #
    # GHC 8.10
    #
    "8.10.1" = buildGhc {
      version = "8.10.1";
      sha256 = "sha256:1jhs396lww687121mdvb723n8ql1rv60x9s3xxy7c9h749gy1n75";
      bootGhc = artefacts.ghc_8_8_4;
    };
    # 8.10.2 is currently broken
    #"8.10.2" = buildGhc {
    #  version = "8.10.2";
    #  sha256 = "sha256:03g5k48cm8758fz4y3yv14r5p46cxcspjwy5flr6b1s49ciy2s79";
    #  bootGhc = artefacts.ghc_8_8_4;
    #};
    "8.10.3" = buildGhc {
      version = "8.10.3";
      sha256 = "sha256:0phgyrmmnni6cpzah7pgx9plql84bc24c4yhrjvcw2ksk3lncx0g";
      bootGhc = artefacts.ghc_8_8_4;
    };
    "8.10.4" = buildGhc {
      version = "8.10.4";
      sha256 = "sha256:1blpmp67i0lb1pcmwg8lpf6cg3mpim3adzg8fjmrvil8yr4wp1w9";
      bootGhc = artefacts.ghc_8_8_4;
    };
    "8.10.5" = buildGhc {
      version = "8.10.5";
      sha256 = "sha256:13517aqvrkkpvdk94g7x3q72p2sbjh4qch2yx204zd6prb51hlk1";
      bootGhc = artefacts.ghc_8_8_4;
    };
    "8.10.6" = buildGhc {
      version = "8.10.6";
      sha256 = "sha256:00ccsqm4hrb1ffqxib8c69wyj11b1f71yhhhkkhj57cnahfxp7ii";
      bootGhc = artefacts.ghc_8_8_4;
    };
    "8.10.7" = buildGhc {
      version = "8.10.7";
      sha256 = "sha256:1n467cknqhm07b471j3hsdpz5cgny5552ydf3kspk7mid1bapyfi";
      bootGhc = artefacts.ghc_8_8_4;
    };

    #
    # GHC 9.0
    #
    "9.0.1" = buildGhc {
      version = "9.0.1";
      url = "https://downloads.haskell.org/ghc/9.0.1/ghc-9.0.1-src.tar.xz";
      sha256 = "sha256:1wp240i99gccs21agjq0249lv6raffblzqkl4vpnlj290dycwb9h";
      bootGhc = artefacts.ghc_8_10_7;
      patches = [ ./patches/T19655.patch ];
    };

    "9.0.2" = buildGhc {
      version = "9.0.2";
      url = "https://downloads.haskell.org/ghc/9.0.2/ghc-9.0.2-src.tar.xz";
      sha256 = "sha256:19647slxmbx1rzz61kfsd0cjixp076rz3bd3c89dhixz41xj469c";
      bootGhc = artefacts.ghc_8_10_7;
    };

    #
    # GHC 9.2
    #
    "9.2.1-rc1" = buildGhc {
      version = "9.2.1-rc1";
      url = "https://downloads.haskell.org/ghc/9.2.1-rc1/ghc-9.2.0.20210821-src.tar.xz";
      sha256 = "sha256:1xmkaa58l67lml2f34bvzcckilqr8blqyhs1b0hpkngh26w8q482";
      bootGhc = artefacts.ghc_8_10_7;
    };

    "9.2.1" = buildGhc {
      version = "9.2.1";
      url = "https://downloads.haskell.org/ghc/9.2.1/ghc-9.2.1-src.tar.xz";
      sha256 = "sha256:1ag4ymx9x54hib7vbwp1z6gz6j88lnq9zjia7x78z8r0jk2wjd7l";
      bootGhc = artefacts.ghc_8_10_7;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.2.2" = buildGhc {
      version = "9.2.2";
      url = "https://downloads.haskell.org/ghc/9.2.2/ghc-9.2.2-src.tar.xz";
      sha256 = "sha256:0vvqm2j26am9568z345jracp4gk8x33cz7mbp18zfwagqxizc3yf";
      bootGhc = artefacts.ghc_8_10_7;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
    "9.2.3" = buildGhc {
      version = "9.2.3";
      url = "https://downloads.haskell.org/ghc/9.2.3/ghc-9.2.3-src.tar.xz";
      sha256 = "sha256:03h86p6ciidylwghgkfhgxdw9y8yb7f6hhpn434v8wrj8ryvq5r8";
      bootGhc = artefacts.ghc_8_10_7;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
    "9.2.4" = buildGhc {
      version = "9.2.4";
      url = "https://downloads.haskell.org/ghc/9.2.4/ghc-9.2.4-src.tar.xz";
      sha256 = "sha256:1bnp0z0nzx0ydhdzn4k9crswi2p4z1a3agcara18c1bbfp1mzc97";
      bootGhc = artefacts.ghc_8_10_7;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
    "9.2.5" = buildGhc {
      version = "9.2.5";
      url = "https://downloads.haskell.org/ghc/9.2.5/ghc-9.2.5-src.tar.xz";
      sha256 = "sha256:0rjy1ixf4j3x8rvw8ng1jij6nn4hy0zj0ss1baibqkn6ymwlsyw3";
      bootGhc = artefacts.ghc_8_10_7;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
    "9.2.6" = buildGhc {
      version = "9.2.6";
      url = "https://downloads.haskell.org/ghc/9.2.6/ghc-9.2.6-src.tar.xz";
      sha256 = "sha256:1wl3kk844h88axhwry4xfa749qi3rz71rq7nkvkymcpf58d4yga0";
      bootGhc = artefacts.ghc_8_10_7;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
    "9.2.7" = buildGhc {
      version = "9.2.7";
      url = "https://downloads.haskell.org/ghc/9.2.7/ghc-9.2.7-src.tar.xz";
      sha256 = "sha256:1gp30bs3f2j11xj6azxscbsn8llcz2cvbyag6whcg73d1y0p3apc";
      bootGhc = artefacts.ghc_8_10_7;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
    "9.2.8" = buildGhc {
      version = "9.2.8";
      url = "https://downloads.haskell.org/ghc/9.2.8/ghc-9.2.8-src.tar.xz";
      sha256 = "sha256:00q6azsawfbgalvqbwrm5icgrisbf5vbs2zakxgiibacb3c5z4nc";
      bootGhc = artefacts.ghc_8_10_7;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    #
    # master
    #
    "master" = buildGhc {
      version = "9.5.0";
      src = sources.ghc-master;
      sphinx = python3Packages.sphinx;
      bootGhc = artefacts.ghc_9_2_1;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
  };

  #
  # Experimental Hadrian derivations
  #
  ghcs_hadrian = {
    #
    # GHC 9.0
    #
    # N.B. hadrian's installation support is broken in 9.0.1.

    #"9.0.1" = buildGhcHadrian {
    #  version = "9.0.1";
    #  sha256 = "sha256:1wp240i99gccs21agjq0249lv6raffblzqkl4vpnlj290dycwb9h";
    #  bootVer = "ghc8101";
    #  libffi = null; # FIXME: hadrian's with-system-libffi support is broken
    #};

    #
    # GHC 9.2 (experimental Hadrian derivation)
    #
    "9.2.1-rc1" = buildGhcHadrian {
      version = "9.2.1-rc1";
      url = "https://downloads.haskell.org/ghc/9.2.1-rc1/ghc-9.2.0.20210821-src.tar.xz";
      sha256 = "sha256:1xmkaa58l67lml2f34bvzcckilqr8blqyhs1b0hpkngh26w8q482";
      bootGhc = artefacts.ghc_8_10_7;
      hadrianBootstrapOpts = {
        shakeVersion = "0.18.5";
        haskellPkgs = haskell.packages.ghc8107;
      };
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.2.1" = buildGhcHadrian {
      version = "9.2.1";
      url = "https://downloads.haskell.org/ghc/9.2.1/ghc-9.2.1-src.tar.xz";
      sha256 = "sha256:1ag4ymx9x54hib7vbwp1z6gz6j88lnq9zjia7x78z8r0jk2wjd7l";
      bootGhc = artefacts.ghc_8_10_7;
      hadrianBootstrapOpts = {
        shakeVersion = "0.18.5";
        haskellPkgs = haskell.packages.ghc8107;
      };
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.2.2" = buildGhcHadrian {
      version = "9.2.2";
      url = "https://downloads.haskell.org/ghc/9.2.2/ghc-9.2.2-src.tar.xz";
      sha256 = "sha256:0vvqm2j26am9568z345jracp4gk8x33cz7mbp18zfwagqxizc3yf";
      bootGhc = artefacts.ghc_8_10_7;
      hadrianBootstrapOpts = {
        shakeVersion = "0.18.5";
        haskellPkgs = haskell.packages.ghc8107;
      };
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.2.3" = buildGhcHadrian {
      version = "9.2.3";
      url = "https://downloads.haskell.org/ghc/9.2.3/ghc-9.2.3-src.tar.xz";
      sha256 = "sha256:03h86p6ciidylwghgkfhgxdw9y8yb7f6hhpn434v8wrj8ryvq5r8";
      bootGhc = artefacts.ghc_8_10_7;
      hadrianBootstrapOpts = {
        shakeVersion = "0.18.5";
        haskellPkgs = haskell.packages.ghc8107;
      };
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.2.4" = buildGhcHadrian {
      version = "9.2.3";
      url = "https://downloads.haskell.org/ghc/9.2.4/ghc-9.2.4-src.tar.xz";
      sha256 = "sha256:1bnp0z0nzx0ydhdzn4k9crswi2p4z1a3agcara18c1bbfp1mzc97";
      bootGhc = artefacts.ghc_8_10_7;
      hadrianBootstrapOpts = {
        shakeVersion = "0.18.5";
        haskellPkgs = haskell.packages.ghc8107;
      };
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.2.5" = buildGhcHadrian {
      version = "9.2.5";
      url = "https://downloads.haskell.org/ghc/9.2.5/ghc-9.2.5-src.tar.xz";
      sha256 = "sha256:0rjy1ixf4j3x8rvw8ng1jij6nn4hy0zj0ss1baibqkn6ymwlsyw3";
      bootGhc = artefacts.ghc_8_10_7;
      hadrianBootstrapOpts = {
        shakeVersion = "0.18.5";
        haskellPkgs = haskell.packages.ghc8107;
      };
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.2.6" = buildGhcHadrian {
      version = "9.2.6";
      url = "https://downloads.haskell.org/ghc/9.2.6/ghc-9.2.6-src.tar.xz";
      sha256 = "sha256:1wl3kk844h88axhwry4xfa749qi3rz71rq7nkvkymcpf58d4yga0";
      bootGhc = artefacts.ghc_8_10_7;
      hadrianBootstrapOpts = {
        shakeVersion = "0.18.5";
        haskellPkgs = haskell.packages.ghc8107;
      };
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    #
    # GHC 9.4
    #
    "9.4.1-alpha1" = buildGhcHadrian {
      version = "9.4.1-alpha1";
      url = "https://downloads.haskell.org/ghc/9.4.1-alpha1/ghc-9.4.0.20220501-src.tar.xz";
      sha256 = "sha256:042wllyi6wbv9zyynjiak3vvky5k12brf7f9yyl7zb0dvgdaa98w";
      bootGhc = ghcs."9.0.1";
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_7;
      buildFlavour = "perf"; # the `release` flavour wasn't around yet
    };

    "9.4.1-alpha2" = buildGhcHadrian {
      version = "9.4.1-alpha2";
      url = "https://downloads.haskell.org/~ghc/9.4.1-alpha2/ghc-9.4.0.20220523-src.tar.xz";
      sha256 = "sha256:1w4cx8xxz24m7fv86nmai0ikwd36vhsdw9ni4c8wkl8rq07iacaj";
      bootGhc = ghcs."9.2.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.4.1-alpha3" = buildGhcHadrian {
      version = "9.4.1-alpha3";
      url = "https://downloads.haskell.org/~ghc/9.4.1-alpha3/ghc-9.4.0.20220623-src.tar.xz";
      sha256 = "sha256:05faax14llf88mch3z4xkkr0zafdy2hml6w0rcnh0nxnk1k8al4i";
      bootGhc = ghcs."9.2.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.4.1-rc1" = buildGhcHadrian {
      version = "9.4.1-rc1";
      url = "https://downloads.haskell.org/ghc/9.4.1-rc1/ghc-9.4.0.20220721-src.tar.xz";
      sha256 = "sha256:1rkrzq9idb80g8hm60qncb9dyi9wdj38xcv7ykk71s28s3fkc5yb";
      bootGhc = ghcs."9.2.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.4.1" = buildGhcHadrian {
      version = "9.4.1";
      url = "https://downloads.haskell.org/ghc/9.4.1/ghc-9.4.1-src.tar.xz";
      sha256 = "sha256:0c0zggx5plnmiml3q8ma5lj5h94ll308gjk5jxb87b7zhz73nk4s";
      bootGhc = ghcs."9.2.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.4.2" = buildGhcHadrian {
      version = "9.4.2";
      url = "https://downloads.haskell.org/ghc/9.4.2/ghc-9.4.2-src.tar.xz";
      sha256 = "sha256:0g89a0j97fpks2n6casy6g46j5p5nfd9095fn7js04ri29hiwra5";
      bootGhc = ghcs."9.2.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.4.3" = buildGhcHadrian {
      version = "9.4.3";
      url = "https://downloads.haskell.org/ghc/9.4.3/ghc-9.4.3-src.tar.xz";
      sha256 = "sha256:1ql3pk6yakllkj9lny198jqm2h76bi9ip2wm5nmpvcl674a5w9ai";
      bootGhc = ghcs."9.2.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.4.4" = buildGhcHadrian {
      version = "9.4.4";
      url = "https://downloads.haskell.org/ghc/9.4.4/ghc-9.4.4-src.tar.xz";
      sha256 = "sha256:1fwxkbd0lbhx167lkg836c9z6xz21rj506jffhgzxjgrj3bf0khb";
      bootGhc = ghcs."9.2.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.4.5" = buildGhcHadrian {
      version = "9.4.5";
      url = "https://downloads.haskell.org/ghc/9.4.5/ghc-9.4.5-src.tar.xz";
      sha256 = "sha256:08jwvqgrlga3y1v3mgvqq9jy5in85izfwfjdl8k1cfcaqv01wx56";
      bootGhc = ghcs."9.2.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
   # See https://gitlab.haskell.org/bgamari/ghcs-nix/-/issues/3
   # "9.4.6" = buildGhcHadrian {
   #   version = "9.4.6";
   #   url = "https://downloads.haskell.org/ghc/9.4.6/ghc-9.4.6-src.tar.xz";
   #   sha256 = "sha256:0kf34aiypw3v00ywxn92kr73k3ls3ivb5a6pawwvk59sb9yznxyf";
   #   bootGhc = ghcs."9.2.2";
   #   happy = happy-1_20_0;
   #   alex = alex-3_2_7;
   # };
    "9.4.7" = buildGhcHadrian {
      version = "9.4.7";
      url = "https://downloads.haskell.org/ghc/9.4.7/ghc-9.4.7-src.tar.xz";
      sha256 = "sha256:1lbv0n8kz56l2i0i1vfbpprmggbml2x10q7hyb9x6wbwygsc5xvj";
      bootGhc = ghcs."9.2.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.6.1-alpha1" = buildGhcHadrian {
      version = "9.6.1-alpha1";
      url = "https://downloads.haskell.org/ghc/9.6.1-alpha1/ghc-9.6.0.20230111-src.tar.xz";
      sha256 = "sha256:10gj2rn41s4d6lzn73q1ycgm7ygwi2w943d531n5nqv9k6g70ds4";
      bootGhc = ghcs."9.4.3";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.6.1-alpha2" = buildGhcHadrian {
      version = "9.6.1-alpha2";
      url = "https://downloads.haskell.org/ghc/9.6.1-alpha2/ghc-9.6.0.20230128-src.tar.xz";
      sha256 = "sha256:05ajviascjbwk7rznaszan13iznbs8d5wv22xznc994bvqyf5n9m";
      bootGhc = ghcs."9.4.3";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.6.1-alpha3" = buildGhcHadrian {
      version = "9.6.1-alpha3";
      url = "https://downloads.haskell.org/ghc/9.6.1-alpha3/ghc-9.6.0.20230210-src.tar.xz";
      sha256 = "sha256:11ab98vzmpn598hnqbxpkcmpjwf7rnpaisifrqrra31p3migixsv";
      bootGhc = ghcs."9.4.3";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.6.1-rc1" = buildGhcHadrian {
      version = "9.6.1-rc1";
      url = "https://downloads.haskell.org/ghc/9.6.1-rc1/ghc-9.6.0.20230302-src.tar.xz";
      sha256 = "sha256:050j0dfs0swqcm11s948qhcpb9xgk6nx8gf75lagl7hnyikcw35q";
      bootGhc = ghcs."9.4.3";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.6.1" = buildGhcHadrian {
      version = "9.6.1";
      url = "https://downloads.haskell.org/ghc/9.6.1/ghc-9.6.1-src.tar.xz";
      sha256 = "sha256:1xwycm7r4p6m22qwyfk7ibdh1q00r00zmy7759x2jsjlxnvjprs4";
      bootGhc = ghcs."9.4.3";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.6.2" = buildGhcHadrian {
      version = "9.6.2";
      url = "https://downloads.haskell.org/ghc/9.6.2/ghc-9.6.2-src.tar.xz";
      sha256 = "sha256:01h1hw834xy4lgam6pn6k2xd0vd0x4sdzxd0gh7wwzzldqcv052v";
      bootGhc = ghcs."9.4.3";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.6.3" = buildGhcHadrian {
      version = "9.6.3";
      url = "https://downloads.haskell.org/ghc/9.6.3/ghc-9.6.3-src.tar.xz";
      sha256 = "sha256:13gp8hsrgscn3w9aa8ihqjpr5viwdac9qhyj9q4bnfzzj0wmss90";
      bootGhc = ghcs."9.4.3";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };

    "9.8.1-alpha1" = buildGhcHadrian {
      version = "9.8.1-alpha1";
      url = "https://downloads.haskell.org/ghc/9.8.1-alpha1/ghc-9.8.0.20230727-src.tar.xz";
      sha256 = "sha256:0lcl6rawmagf1vsv9piyc2hmjy6pvwx6iv2kcd2khwqlj9fbilya";
      bootGhc = ghcs."9.6.1";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
    "9.8.1-alpha2" = buildGhcHadrian {
      version = "9.8.1-alpha2";
      url = "https://downloads.haskell.org/ghc/9.8.1-alpha2/ghc-9.8.0.20230809-src.tar.xz";
      sha256 = "sha256:1ngvmhsbpzi9pawc5l79k51qnf8f3l40kx8jb46qn4xgr6aa3l4b";
      bootGhc = ghcs."9.6.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
    "9.8.1-alpha3" = buildGhcHadrian {
      version = "9.8.1-alpha3";
      url = "https://downloads.haskell.org/ghc/9.8.1-alpha3/ghc-9.8.0.20230822-src.tar.xz";
      sha256 = "sha256:08s0alx1n0hhg8ppzlz3m5hjf3jp7q54127pp6d2garfkm14nsy5";
      bootGhc = ghcs."9.6.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
    "9.8.1-alpha4" = buildGhcHadrian {
      version = "9.8.1-alpha4";
      url = "https://downloads.haskell.org/ghc/9.8.1-alpha4/ghc-9.8.0.20230919-src.tar.xz";
      sha256 = "sha256:0zp1apbv5n0ngnrhcx7a20871lphwly86fpwvph529fj51s1fas3";
      bootGhc = ghcs."9.6.2";
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };


    #
    # master
    #
    "master" = buildGhc {
      version = "9.5.0";
      src = sources.ghc-master;
      sphinx = python3Packages.sphinx;
      bootGhc = artefacts.ghc_9_2_1;
      happy = happy-1_20_0;
      alex = alex-3_2_7;
    };
  };

  #
  # Cabal-install
  #
  buildCabal = callPackage ./cabal-install.nix {} ghcs;

  cabals = {
    "3.4.0.0" = buildCabal { version = "cabal-install-3.4.0.0";
                sha256 = "0q32issgbg57rv60rriacqw5bsrxsskb9v1xbm6py72piyy2lq6k";
                bootVer = "8.10.1";
              };
    "3.6.2.0" = buildCabal { version = "Cabal-v3.6.2.0";
                sha256 = "04sbn42wxwx46b6vg658nqmjbv23lr5y8mavhhiwp0s3kjlkjj3a";
                bootVer = "8.10.7";
              };
    "3.8.1.0" = buildCabal { version = "cabal-install-v3.8.1.0";
                sha256 = "sha256-8fbur+Vmu2kkaj6W2k0h/Pn6cCT9euJF+xrg6r9M9YU=";
                bootVer = "8.10.7"; };
    "3.10.1.0" = buildCabal { version = "cabal-install-v3.10.1.0";
                sha256 = "sha256-EcbWhkT2aywt5JfPNfXGSjjdnZAf7DPWdZ6c0fIdn6c=";
                bootVer = "8.10.7"; };
    };


  #
  # Administrivia
  #
  join =
    let
      wrapGhc = version: drv: "ln -s ${drv}/bin/ghc $out/bin/ghc-${version}";
    in runCommand "all-ghcs" {
      } ''
        mkdir -p $out/bin
        ${lib.concatStringsSep "\n" (lib.mapAttrsToList wrapGhc ghcs)}
      '';

  mapNames = f: attrset:
    lib.mapAttrs' (name: value: lib.nameValuePair (f name) value) attrset;
  underscoredVersion = version: lib.replaceStrings ["."] ["_"] version;


  attrs =
    (mapNames (v: "cabal-install-" + underscoredVersion v) cabals)
    // (mapNames (v: "ghc-" + underscoredVersion v) ghcs)
    // (mapNames (v: "ghc-make-" + underscoredVersion v) ghcs_make)
    // (mapNames (v: "ghc-hadrian-" + underscoredVersion v) ghcs_hadrian);

  cabal-install = attrs.cabal-install-3_8_1_0;

in
  {
    inherit nixpkgs join cabal-install ghcs ghcs_make ghcs_hadrian sources;

    # derivations to be built by CI
    ciDrvs = attrs;
  } // attrs
